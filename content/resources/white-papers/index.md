---
title: "White Papers"
date: 2024-04-03
seo_title: "White Papers | Software Defined Vehicle"
tags: ["white papers", "study", "automotive"]
container: "container"
---

{{< newsroom/resources wg="sdv" type="white_paper" template="cover" >}}
