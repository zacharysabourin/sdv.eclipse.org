---
title: About the Working Group
seo_title: About the Working Group | Software Defined Vehicle
headline: Join the Software Defined Vehicle Working Group
hide_page_title: true
hide_sidebar: true
header_wrapper_class: 'header-alt'
custom_jumbotron: |
      <div>
        <h1>About the Working Group</h1>
      </div>       
---

{{< grid/div class="container margin-bottom-10" >}}

## What is the SDV Working Group? { .h1 .margin-bottom-30 .margin-top-20 .text-center }

The Eclipse Software Defined Vehicle (SDV) is a Working Group within the [Eclipse Foundation](https://www.eclipse.org/) that facilitates open source development of automotive software. The aim is to provide a forum for individuals and organizations to build and promote open source solutions for worldwide automotive industry markets. Using a “code first” approach, SDV-related projects focus on building the industry’s first open source software stacks and associated tooling for the core functionality of a new class of automobile.

The SDV Working Group is designed as a vendor-neutral, member-driven organization that allows users and developers to define the roadmap collaboratively. The Working Group provides a governance framework for open source IP management and collaboration. This includes a Steering Committee that defines and manages the strategy and technical roadmap of the working group. A Technical Advisory Committee recommends which Eclipse Foundation open source projects should be included within the purview of the working group.

{{</ grid/div >}}

{{< grid/div class="text-center margin-top-30 margin-bottom-30" isMarkdown="false">}}

<a class="btn btn-primary" href="https://www.eclipse.org/org/workinggroups/sdv-charter.php">Read the Charter</a>
<a class="btn btn-primary" href="/membership">Become a Member</a>

{{</ grid/div >}}
