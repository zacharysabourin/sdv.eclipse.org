---
title: Become a Member
seo_title: Become a Member | Software Defined Vehicle
headline: Join the Software Defined Vehicle Working Group
hide_page_title: true
hide_sidebar: true
page_css_file: public/css/become-a-member.css
header_wrapper_class: 'header-alt'
custom_jumbotron: |
      <div>
        <h1>Join the Software Defined Vehicle Working Group</h1>
      </div>       
---

{{< pages/about/why-should-you-join >}}

{{< pages/about/testimonials >}}

{{< grid/div class="container" id="contact-us" isMarkdown="false">}}
<div class="row row-no-gutters">
  <div class="col-sm-20 col-xs-24 col-sm-offset-2">
    <div class="card-contact">
      <h2>Contact us about Membership</h2>
      {{< hubspot_contact_form portalId="5413615" formId="8bf9c8c1-32c8-4e37-8956-b7420e3388cb" >}}
    </div>
  </div>
</div>
{{</ grid/div >}}