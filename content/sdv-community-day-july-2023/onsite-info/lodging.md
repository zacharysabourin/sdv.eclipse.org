---
title: Lodging
weight: 2
---

[Hotel Knoblauch Friedrichshafen](https://www.hotel-knoblauch.de/en/)

[Styles Hotel Friedrichshafen](https://www.styles-hotel-friedrichshafen.de/de/)

[Hotel Merian](https://merian-fn.de/)
