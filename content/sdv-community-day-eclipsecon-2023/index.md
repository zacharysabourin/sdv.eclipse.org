---
title: SDV Community Day at EclipseCon
date: 2023-08-041T00:00:00-04:00
summary: >-
    Meet and engage with the thriving SDV Community and work towards building
    an SDV Distribution entirely consisting of Open Source Projects.
description: >-
    Meet and engage with the thriving SDV Community and work towards building
    an SDV Distribution entirely consisting of Open Source Projects.
categories: []
keywords: []
headline: ''
custom_jumbotron: |
    <h1 class="featured-jumbotron-title text-center">SDV Community Day at EclipseCon - October 2023</h1>
    <p class="featured-jumbotron-subtitle text-center">
      Join the vibrant Eclipse SDV community as we collaborate to define and
      build the future of mobility and open source software
    </p>
    <div class="more-detail jumbotron-register-container text-center">
        <div class="more-detail-text padding-top-40">
            <p>Onsite Event | Oct 16-17, 2023</p>
            <p class="data-location">
              Forum am Schlosspark | Ludwigsburg, Germany
            </p>       
        </div>
    </div>
hide_page_title: true
hide_headline: true
hide_sidebar: true
hide_breadcrumb: true
header_wrapper_class: header-community-day-2023
container: container-fluid sdv-contribution-day-2022-container
page_css_file: /public/css/sdv-community-day-eclipsecon-2023.css
layout: single
resources:
  - src: "*.md"
---

<!-- About the Event -->
{{< grid/section-container id="registration" containerClass="padding-bottom-40 padding-top-40 featured-section-row" >}}
  {{< grid/div class="container" >}}

## About the Event

The Automotive and Eclipse SDV Community Day offers a 1.5-day experience of
talks, demos, and presentations showcasing projects within the Eclipse
Foundation and SDV Working Group communities.

Day 1 kicks off on Monday, Oct 16 with an overview of various Eclipse
Foundation automotive endeavors. This aims to align Eclipse SDV community
projects with other automotive initiatives and working groups like Eclipse
Theia, Eclipse Zenoh, Catena-X, and more

Day 2 (morning only) is dedicated solely to Eclipse SDV Working Group
initiatives and open collaborations

This event is open to developers, product managers, architects, and business
managers interested in the software-defined vehicle. The SDV Working Group's
mission is to promote cross-industry collaboration, while establishing an open
technology platform for future software-defined vehicles. The community
embraces a "code first" strategy, facilitating agile software development and
faster time-to-market for member organizations.

  {{</ grid/div >}}
{{</ grid/section-container >}}

<!-- How to Attend -->

{{< grid/section-container id="registration" containerClass="padding-bottom-40" >}}
  {{< grid/div class="container" isMarkdown="false" >}}
    {{< events/registration event="sdv-community-day-eclipsecon-2023" title="How to Attend" >}}

All participants will need a ticket to attend the Automotive and SDV Community
Day at EclipseCon. The options are as follows:

- A €40 (plus VAT) Community Day Only Pass is available. If also attending
  EclipseCon, the fee is €25 (plus VAT).
- Speakers attending Community Day receive a free pass, which covers the
  registration fee.
- If attending both Community Day and EclipseCon, select the All-Access Pass
  during registration, priced at €25 plus VAT. The All-Access Pass cost varies
  by registration date (please check the Registration page for the current
  rate).

    {{</ events/registration >}}
  {{</ grid/div >}}
{{</ grid/section-container >}}

<!-- Timeline -->
{{< grid/div class="container padding-top-40 padding-bottom-40" isMarkdown="false" >}}

  <h2 id="timeline" class="margin-bottom-30">Q3 Community Day Timeline</h2>
  {{< figure imgClass="img img-responsive" src="./images/timeline.jpg" >}}
  <a class="text-center visible-xs" href="./images/timeline.jpg" target="_blank">
    View Full Size
  </a>

{{</ grid/div >}}

<!-- Agenda -->
{{< grid/section-container id="agenda" class="featured-section-row featured-section-row-light-bg" title="Agenda" >}}
  {{< pages/sdv-community-day-2023/agenda event="sdv-community-day-eclipsecon-2023" icon_class="fa-download" navLabel="Select Day" >}}
  <small>* All times displayed are in Central European Summer Time (CEST).</small>
  {{< bootstrap/modal id="eclipsefdn-modal-event-session" >}}
{{</ grid/section-container >}}

<!-- Co-host -->
{{< grid/section-container containerClass="container padding-top-60 padding-bottom-40 text-center" isMarkdown="false" >}}
  {{< grid/div >}}

## Co-hosted at EclipseCon 2023

SDV Community Days is hosted by the Eclipse Software Defined Vehicle Working
Group.

  {{</ grid/div >}}
{{</ grid/section-container >}}

<!-- Participating Members -->
{{< grid/section-container containerClass="container padding-bottom-40 text-center" isMarkdown="false" >}}
  <h2 id="participating>Thanks to Our Participating Member Companies</h2>
  <ul class="eclipsefdn-members-list list-inline margin-30 flex-center gap-40" data-ml-wg="sdv" data-ml-template="only-logos"></ul>
{{</ grid/section-container >}}

<!-- Onsite Info -->
{{< grid/div class="featured-section-row-light-bg padding-bottom-60 padding-top-40 text-center" isMarkdown="false" >}}
  {{< grid/div class="container" isMarkdown="false" >}}
    <h2>Information for Onsite Attendees</h2>
    {{< info_list resource="onsite-info/*.md" >}}
  {{</ grid/div >}}
{{</ grid/div >}}
